import csv
import pdb
import random
import numpy as np
from datetime import datetime
from sklearn.ensemble import AdaBoostClassifier
from sklearn.tree import DecisionTreeRegressor

# train_file = 'refined_train.csv'
# test_file = 'preprocess_test.csv'
train_file = 'preprocess_train_lr_shuffle.csv'
test_file = 'preprocess_test_lr_shuffle_small.csv'
submission = 'output.csv'

print "start loading...%s"%str(datetime.now())
with open(train_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    dataset = [row for row in reader]
# dataset = [map(int,dataset[i]) for i in xrange(len(dataset))]
dataset = np.array(dataset)
random.shuffle(dataset)
train_x = [row[4:] for row in dataset]
train_y = [row[3] for row in dataset]
train_x = [map(int,row) for row in train_x]
train_y = map(int,train_y)
print len(train_x)
print len(train_y)
dataset_test = []
with open(test_file, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    dataset_test = [row for row in reader]
#    dataset_test = [map(int,dataset_test[i]) for i in xrange(len(dataset_test))]
dataset_test = np.array(dataset_test)
test_x = [map(int, row[4:]) for row in dataset_test]
test_id = [row[2] for row in dataset_test]
print len(test_x)
print "start training...%s"%str(datetime.now())
# Construct dataset
# Create and fit an AdaBoosted decision tree
bdt = AdaBoostClassifier(DecisionTreeRegressor(max_depth=5),
                         algorithm="SAMME",
                         n_estimators=200)

bdt.fit(train_x, train_y)

predict = bdt.predict(test_x)

print "start writing files...%s"%str(datetime.now())
with open(submission, 'w') as outfile:
    outfile.write('id,click\n')
    for i in xrange(len(test_id)):
        outfile.write('%s,%s\n' % (str(test_id[i]), str(predict[i])))



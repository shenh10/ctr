'''
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
'''


from datetime import datetime
import csv 
from math import exp, log, sqrt
import random
import numpy as np
from scipy.optimize import fmin_l_bfgs_b

# TL; DR, the main training process starts on line: 250,
# you may want to start reading the code from there


##############################################################################
# parameters #################################################################
##############################################################################

# A, paths
train = 'preprocess_train_cgl_small.csv'               # path to training file
test = 'preprocess_test_lr_shuffle_small.csv'                 # path to testing file
submission = 'submission0115_28_orgPara_shuffled_noval_epoch4.csv'  # path of to be outputted submission file

# C, feature/hash trick
D = 2 ** 26             # number of weights to use
k = 50
mylambda = 1

class cgl(object):
    def __init__(self, k, mylambda1, D):
        self.k = k
        self.mylambda = mylambda
        self.D = D 
        self.w = (np.random.random((1,D*k))).tolist()
        self.v = (np.random.random((1,D*k))).tolist()
        self.b = [0.] * D
        self.epsilon = .1

    def _indices(self, x):
        ''' A helper generator that yields the indices in x

            The purpose of this generator is to make the following
            code a bit cleaner when doing feature interaction.
        '''

        # first yield index of the bias term
        yield 0

        # then yield the normal indices
        for index in x:
            yield index

    def train(self, x, y):
        threshold = 1e-3
        loop = 1
        while 1:
            print "Loop %d..."%loop
            print "training w, b .."+ str(datetime.now())
            vec1 =fmin_l_bfgs_b(self.targetFunc, self.w+self.b, fprime=self.targetDerWB, args=(x,y),iprint=1)
            self.w = vec1[:(D*k)]
            self.b = vec1[(D*k):]
            print "training v, b .."+ str(datetime.now())
            [ vec2, value ]=fmin_l_bfgs_b(self.targetFunc, self.v+self.b, fprime=self.targetDerVB, args=(x,y),iprint=1)
            self.v = vec2[:(D*k)]
            self.b = vec2[(D*k):]
            if loop == 1:
                oldValue = value
                continue
            if abs(value- oldValue)< threshold:
                break
            else:
                oldValue = value
            loop += 1

    def targetFunc(self, x, y):
        w = self.w
        v = self.v
        mylambda = self.mylambda
        D = self.D
        k = self.k
        norm_w = 0.
        norm_v = 0.
        for i in xrange(D):
            norm_w = (np.sqrt((np.array(w[i*k, (i+1)*k] ) ** 2).sum())).sum()
            norm_v = (np.sqrt((np.array(v[i*k, (i+1)*k] ) ** 2).sum())).sum()
        sum_ = mylambda*(norm_w+norm_v)
        for i in xrange(len(x)):
            sum_ += logloss(predict(x[i]), y[i])
        return sum_
  
    def targetDerWB(self,x, y):
        w = self.w
        v = self.v
        mylambda = self.mylambda
        D = self.D
        k = self.k
        der_b = [0.]*D
        epsilon = self.epsilon
        der_w = np.array([0.]*(D*k))
        der_b = np.array([0.]*(D))
        for i in xrange(len(x)):
            y_pre = predict(x[i])
            [tmp_a, tmp_u, tmp_b] =  self.crossProduct(x[i])      
            for xi in self._indices(x[i]):
                if index in [2,3,4,5,6,7,8]:
                    continue
                elif index in [9,10,11,12,13]:
                    der_w[(xi-D):(xi-D)+k] += (y_pre-y[i])* tmp_a 
                else:
                    der_b[(xi-2*D)] += (y_pre-y[i] )

        norm_w = np.array([0.]*(D*k))
        for i in xrange(D):
            norm_w = np.array(w)/np.sqrt((w[i*k, (i+1)*k]**2).sum()+epsilon)

        return (mylambda*norm_w + der_w).tolist()+der_b.tolist()

    def targetDerVB(self,x, y):
        w = self.w
        v = self.v
        mylambda = self.mylambda
        D = self.D
        k = self.k
        der_b = [0.]*D
        epsilon = self.epsilon
        der_w = np.array([0.]*(D*k))
        der_b = np.array([0.]*(D))
        for i in xrange(len(x)):
            y_pre = predict(x[i])
            [tmp_a, tmp_u, tmp_b] =  self.crossProduct(x[i])      
            for xi in self._indices(x[i]):
                if index in [2,3,4,5,6,7,8]:
                    der_v[(xi):(xi)+k] += (y_pre-y[i])* tmp_a 
                elif index in [9,10,11,12,13]:
                    continue
                else:
                    der_b[(xi-2*D)] += (y_pre-y[i] )

        norm_v = np.array([0.]*(D*k))
        for i in xrange(D):
            norm_v = np.array(v)/np.sqrt((v[i*k, (i+1)*k]**2).sum()+epsilon)

        return (mylambda*norm_v + der_v).tolist()+der_b.tolist()

    def predict(self, x):
        [tmp_a, tmp_u, tmp_b] =  self.crossProduct(x)
        ker = (tmp_a*tmp_u).sum()+tmp_b
        return 1. / (1. + exp(-max(min(ker, 35.), -35.)))


    def crossProduct(self, x):
        w = self.w
        v = self.v
        b = self.b
        k = self.k 
        tmp_u = np.array([0.]*k)
        tmp_a = np.array([0.]*k)
        tmp_b = 0.
        index = 0
        for i  in self._indices(x):
            if index in [2,3,4,5,6,7,8]:
                tmp_a += np.array(v[i*k:(i+1)*k] ) 
            elif index in [9,10,11,12,13]:
                tmp_u += np.array(w[(i-D)*k:(i-D+1)*k] )
            else:
                tmp_b += b[(i-2*D)]
            index += 1
        return tmp_a, tmp_u, tmp_b

def logloss(p, y):
    ''' FUNCTION: Bounded logloss

        INPUT:
            p: our prediction
            y: real answer

        OUTPUT:
            logarithmic loss of p given y
    '''

    p = max(min(p, 1. - 10e-15), 10e-15)
    return -log(p) if y == 1. else -log(1. - p)



def data(path, D):
    ''' GENERATOR: Apply hash-trick to the original csv row
                   and for simplicity, we one-hot-encode everything

        INPUT:
            path: path to training or testing file
            D: the max index that we can hash to

        YIELDS:
            ID: id of the instance, mainly useless
            x: a list of hashed and one-hot-encoded 'indices'
               we only need the index since all values are either 0 or 1
            y: y = 1 if we have a click, else we have y = 0
    '''
    reader = csv.reader(open(path))
    t = 0
    for row in reader:
#        if(not row[3] and random.randint(0,1) > 0):
#            continue
#        else:
         yield t, row[1], row[2], row[4:-1], row[3]
         t = t+1


##############################################################################
# start training #############################################################
##############################################################################

print "Start Program..." +str(datetime.now())

reader = csv.reader(open(train))
print "Start loading data..."+str(datetime.now())
dataSet = [row for row in reader]
for i in xrange(len(dataSet)):
    dataSet[i][3:] = map(int , dataSet[i][3:])
train_feature = dataSet[1:]
train_label = dataSet[0:0]
# initialize ourselves a learner
learner = cgl(k, mylambda, D)
print "Start training data..." +str(datetime.now())
learner.train(train_feature,train_label)
print "training done..." +str(datetime.now())
#print('Epoch %d finished, validation logloss: %f, elapsed time: %s' % (
#e, loss/count, str(datetime.now() - start)))


##############################################################################
# start testing, and build Kaggle's submission file ##########################
##############################################################################

with open(submission, 'w') as outfile:
    outfile.write('id,click\n')
    for t, date, ID, x, y in data(test, D):
        x = map(int,x);
        p = learner.predict(x)
        outfile.write('%s,%s\n' % (ID, str(p)))
print "Done..." +str(datetime.now())
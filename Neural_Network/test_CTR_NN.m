%function test_XinZhongGuan_NN
clear all;close all;clc;
addpath(genpath('DeepLearnToolbox'));
Tr = importdata('preprocess_train_lr_small.csv');
Tt = importdata('preprocess_test_lr_small.csv');

train_x = Tr(:,5:end);
test_x = Tt(:,4:end);
test_id = Tt(:,3);

nsamples = size(train_x,1);

train_y = toOneOfMany(Tr(:,4),2,0);
test_y = [];


rand('state',0)
%num_in = size(Tr.input,2);
%num_out = size(Tr.class_labels,1);
%num_hid = floor(0.3*num_in+0.7*num_out);
nn = nnsetup([22 14 2]);
nn.weightPenaltyL2 = 1e-4;  %  L2 weight decay
nn.learningRate = 1;
%nn.scaling_learningRate = 0.998;            %  Scaling factor for the learning rate (each epoch)
nn.momentum = 0.5;          %  Momentum
nn.inputZeroMaskedFraction = 0;            %  Used for Denoising AutoEncoders
nn.dropoutFraction = 0.5;   %  //Dropout fraction，每一次mini-batch样本输入训练时，随机扔掉50%的隐含层节点
nn.output  = 'softmax';                   %  use softmax output
opts.numepochs =2;        %  //Number of full sweeps through data
opts.batchsize = 200;       %  //Take a mean gradient step over this many samples
opts.plot = 0;              %  enable plotting
train_x = train_x(1:nsamples-mod(nsamples,opts.batchsize),:);
train_y = train_y(1:nsamples-mod(nsamples,opts.batchsize),:);
% normalize
[train_x, mu, sigma] = zscore(train_x);
%test_x = normalize(test_x, mu, sigma);
test_x = zscore(test_x);
% split training data into training and validation data
%randindex = randperm(nsamples-mod(nsamples,opts.batchsize));
% vx   = train_x(randindex(1:200),:);
% tx = train_x(randindex(201:end),:);
% vy   = train_y(randindex(1:200),:);
% ty = train_y(randindex(201:end),:);
 %vx = test_x;
 %tx = train_x;
 %vy = test_y;
 %ty = train_y;
%nn = nntrain(nn, tx, ty, opts, vx, vy);
nn = nntrain(nn, train_x, train_y, opts);
nn.testing = 1;
nn = nnff(nn, test_x, zeros(size(test_x,1), nn.size(end)));
nn.testing = 0;
test_y = nn.a{end};
test_y_prob = test_y(:,2);
testoutput = [test_id test_y_prob];
csvwrite('matlab_CTR.csv', testoutput);
%[er, bad] = nntest(nn, test_x, test_y);
%str = sprintf('testing error rate is: %f',er);
%disp(str)

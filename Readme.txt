Click-Through Rate Prediction

Directory
--------------------
|-CTR
  |-logesticRegression.py Best scripts. Implemented Google's CTR paper.
  |-dataprocess
  	|-data_process_train.py: train data preprocess, save time for multiple testing
    |-split_data.py: split dataset to five sets
  	|-voting_average: combine subsets results
  |-Adaboost_DT 
  	|-adaboost_dt.py : Decision tree with adaboost essemble
  |-CGL
    |-CGL.py CGL implementation , with L-BFGS
    |-cgi_simplify.py: the L-BFGS is memory costy, changed to naive gradient descent with learning rate
  |-Neural_Network
    |- test_CTR_NN.m : Matlab implementation of artificial neural network 
    |- DeepLearnToolbox: Matlab toolbox for deep learning . We used NN package in it  

Best Performance:
  Run logesticRegression.py with following command:
  pypy logesticRegression.py train --train train -o second_module_bit26_inter.gz --bits 26 --L1 2 --L2 2 --interaction  --n_epochs 1  --holdout 1000000

  pypy logesticRegression.py predict --test test -i second_module_bit26_inter.gz -p submission_26_noholdout.csv.gz --bits 26 --L1 2 --L2 2 --interaction  --n_epochs 1  --holdout 1000000 

  Important: Interaction: Add correlated features to improve logestic regression's performance
             Holdout: Set holdout large to avoid lossing too many data in cross validation

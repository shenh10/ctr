
from datetime import datetime
import csv
from math import exp, log, sqrt

# TL; DR, the main training process starts on line: 250,
# you may want to start reading the code from there


##############################################################################
# parameters #################################################################
##############################################################################

# A, paths
train = 'train'               # path to training file
submission = 'preprocess_train_cgl_20.csv'  # path of to be outputted submission file


# C, feature/hash trick
D = 2 ** 20            # number of weights to use

def data(path, D):
    ''' GENERATOR: Apply hash-trick to the original csv row
                   and for simplicity, we one-hot-encode everything

        INPUT:
            path: path to training or testing file
            D: the max index that we can hash to

        YIELDS:
            ID: id of the instance, mainly useless
            x: a list of hashed and one-hot-encoded 'indices'
               we only need the index since all values are either 0 or 1
            y: y = 1 if we have a click, else we have y = 0
    '''
    print "Start loading..."+ str(datetime.now())
    dataSet = []
    reader = csv.reader(open(path))
    first_row = next(reader)
    del first_row[0]
    del first_row[1]
    print "Start read to dataSet ..." + str(datetime.now())
    dataSet = [row for row in reader]
    for t, i in enumerate(xrange(len(dataSet))) :
        row = dataSet[i]
        # process id
        ID = row[0]
        del row[0]
        # process clicks
        y = 0
        if row[0] == '1':
            y = 1
        del row[0]
        
        # extract date
        date = int(row[0][4:6])
        # turn hour really into hoenumerate([(0, 'a'), (1, 'b'), (2, 'c'), (3, 'd'), (4, 'e'), (5, 'f'), (6, 'g'), (7, 'h'), (8, 'i'), (9, 'j')])ur, it was originally YYMMDDHH
        row[0] = row[0][6:]
        # build x
        x = []
        for i in xrange(len(row)):
            value = row[i]
            if  i in [2,3,4,5,6,7,8,]:
            # one-hot encode everything with hash trick
                index = abs(hash(first_row[i] + '_' + value)) % D
                x.append(index)
            elif i in [9,10,11,12,13]:
                # one-hot encode everything with hash trick
                index = abs(hash(first_row[i] + '_' + value)) % D + D
                x.append(index)
            else:
                # one-hot encode everything with hash trick
                index = abs(hash(first_row[i] + '_' + value)) % D + 2*D
                x.append(index)
        yield t, date, ID, x, y



with open(submission, 'w') as outfile:
    for t, date, ID, x, y in data(train, D):
        outfile.write('%s,%s,%s,%s,%s\n' % (t, date, ID, y, str(x)[1:-1]))
print "Done..."+str(datetime.now())

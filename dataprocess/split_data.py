import csv
import pdb
import random
import math
from  datetime import datetime
inputFile = "preprocess_train_lr_shuffle.csv"
outputFile_One = "split_one.csv"
count0 = 0;
count1 = 0;
dataSet = []
targetData = []
D = 26
print "Loading origin data..."+str(datetime.now())
with open(inputFile, 'rb') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    for row in reader:
        if int(row[3]) >0.5:
        	targetData.append(row)
        else:
        	dataSet.append(row)

count1 = len(targetData)
count0 = len(dataSet)
start = 0
N = round(count0*1./count1)
fileLen = math.ceil(count0*1./N)
print "Shuffling zero data..."+str(datetime.now())

random.shuffle(dataSet)
print "Finish  Shuffling zero data..."+str(datetime.now())
def listToStr(data):
    str = ''
    for substr in data:
        str = str+','+substr
    return str[1:]

for i in xrange(N):
    dataTmp = []
    print "Start Writing %sth module file..."%str(i)+str(datetime.now())
    with open('split_module_%s_%s.csv'%(str(D),str(i)), 'w') as outfile:
        dataTmp = dataSet[start:start+int(fileLen)]
        dataTmp = dataTmp+targetData
        random.shuffle(dataTmp)
        for j in xrange(len(dataTmp)):
            outfile.write('%s\n'%(listToStr(dataTmp[j])))
        print start
        start =  start+int(fileLen)


# for i in xrange(N):
#     print "Start Writing %sth zero file..."%str(i)+str(datetime.now())
#     with open('split_zero_%s.csv'%str(i), 'w') as outfile:
#         for j in xrange(start, min(start+fileLen, count0)):
#             outfile.write('%s\n'%str(dataSet[j])[1:-1])
#         start =  start+fileLen

# print "Start Writing One file..."+str(datetime.now())
# random.shuffle(targetData)
# with open(outputFile_One, 'w') as outfile:
# 	for i in xrange(len(targetData)):
# 		outfile.write('%s\n'%str(targetData[i])[1:-1])
# print "Done..."+str(datetime.now())
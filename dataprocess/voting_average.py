import numpypy as np
import csv
from datetime import datetime

N =5 

for i in xrange(N):
	exec 'test%s_file = \'test_26_split%s.csv\''%(str(i),str(i))

submission = 'submission0116_26_voting.csv'
print 'Start loading input...'+str(datetime.now())
for i in xrange(N):
	exec 'dataSet_test%s = [row for row in csv.reader(open(test%s_file))]'%(str(i),str(i))
print 'Start Voting...'+str(datetime.now())
with open(submission, 'w') as outfile:
    outfile.write('id,click\n')
    for i in xrange(len(dataSet_test0)):
		count0 = 0
		count1 = 0
		index0 = []
		index1 = []
		result = 0
		for j in xrange(N):
			if 	eval('float(dataSet_test%d[%d][1])>0.5'%(j,i)):
				count1 = count1 + 1
				index1.append(j)
			else:
				count0 = count0 + 1
				index0.append(j)
		# print count0
		# print count1
		if count0 > count1:
			for k in index0: 
				result += eval('float(dataSet_test%d[%d][1])'%(k,i))
			result = result/count0
			outfile.write('%s,%s\n'%(dataSet_test0[i][0],str(result)))
		else:
			for k in index1: 
				result += eval('float(dataSet_test%d[%d][1])'%(k,i))
			result = result/count1
			outfile.write('%s,%s\n'%(dataSet_test0[i][0],str(result)))
print 'Done...'+str(datetime.now())
